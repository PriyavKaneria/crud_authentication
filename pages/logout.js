import Head from "next/head"
import Image from "next/image"
import styles from "../styles/Home.module.css"
import cookies from "next-cookies"

export async function getServerSideProps(context) {
	const { Authorization } = cookies(context)
	const res = await fetch(`https://crud-authentication-symb.herokuapp.com/`, {
		headers: { Authorization: Authorization },
	})
	if (res.status != 200) {
		return {
			props: {
				is_authenticated: false,
			},
		}
	}
	return {
		props: {
			is_authenticated: true,
		},
	}
}

export default function Home({ is_authenticated }, context) {
	const { Authorization } = cookies(context)
    setTimeout(function(){
        fetch("https://crud-authentication-symb.herokuapp.com/logout", {
            method: "POST",
            credentials: 'include',
            headers: { Authorization: Authorization}
        }).then((res) => {
            return(res)
        })
        .then((res) => {
            console.log(res);
            if (res.status==200) {window.location = "/"}
            return res
        })
    }, 2000);
	return (
		<div className={styles.container}>
			<Head>
				<title>CRUD Auth App</title>
				<meta name='description' content='CRUD and Authentication App' />
				<link rel='icon' href='/favicon.ico' />
				<link
					rel='stylesheet'
					href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css'
				/>
			</Head>

			<section className='hero is-primary is-fullheight'>
				<div className='hero-head'>
					<nav className='navbar'>
						<div className='container'>
							<div id='navbarMenuHeroA' className='navbar-menu'>
								<div className='navbar-end'>
									<a href='/' className='navbar-item'>
										Home
									</a>
									{is_authenticated && (
										<a href='/profile' className='navbar-item'>
											Profile
										</a>
									)}
									{!is_authenticated && (
										<a href='/login' className='navbar-item'>
											Login
										</a>
									)}
									{!is_authenticated && (
										<a href='/signup' className='navbar-item'>
											Sign Up
										</a>
									)}
									{is_authenticated && (
										<a href='/logout' className='navbar-item'>
											Logout
										</a>
									)}
								</div>
							</div>
						</div>
					</nav>
				</div>

				<div className='hero-body'>
					<div className='container has-text-centered'>
						<h1 class='title'>
							Logging you out...
						</h1>
					</div>
				</div>
			</section>
		</div>
	)
}
