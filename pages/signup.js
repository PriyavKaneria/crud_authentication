import Head from "next/head"
import styles from "../styles/Home.module.css"
import { useState } from "react"
import cookies from "next-cookies"

export async function getServerSideProps(context) {
	const { Authorization } = cookies(context)
	const res = await fetch(`https://crud-authentication-symb.herokuapp.com/`, {
		headers: { Authorization: Authorization },
	})
	if (res.status != 200) {
		return {
			props: {
				is_authenticated: false,
			},
		}
	}
	return {
		props: {
			is_authenticated: true,
		},
	}
}

export default function SignUp({ is_authenticated }) {
	const [email, setEmail] = useState("")
	const [name, setName] = useState("")
	const [contact, setContact] = useState("")
	const [password, setPassword] = useState("")
	const [showMessage, setShowMessage] = useState(false)
	function handleSubmit(e) {
		e.preventDefault()
		setShowMessage(false)
		var formData = new FormData()
		formData.append("email", email)
		formData.append("name", name)
		formData.append("contact", contact)
		formData.append("password", password)
		fetch("https://crud-authentication-symb.herokuapp.com/signup", {
			method: "POST",
            credentials: 'include',
			body: formData,
		})
			.then((res) => {
				return res.json()
			})
			.then((res) => {
				if (res.status_code == 200) {
					window.location = "/login"
				} else setShowMessage(true)
				return res
			})
	}
	return (
		<div className={styles.container}>
			<Head>
				<title>CRUD Auth App</title>
				<meta name='description' content='CRUD and Authentication App' />
				<link rel='icon' href='/favicon.ico' />
				<link
					rel='stylesheet'
					href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css'
				/>
			</Head>

			<section className='hero is-primary is-fullheight'>
				<div className='hero-head'>
					<nav className='navbar'>
						<div className='container'>
							<div id='navbarMenuHeroA' className='navbar-menu'>
								<div className='navbar-end'>
									<a href="/" className='navbar-item'>
										Home
									</a>
									{is_authenticated && (
										<a href='/profile' className='navbar-item'>
											Profile
										</a>
									)}
									{!is_authenticated && (
										<a href='/login' className='navbar-item'>
											Login
										</a>
									)}
									{!is_authenticated && (
										<a href='/signup' className='navbar-item'>
											Sign Up
										</a>
									)}
									{is_authenticated && (
										<a href='/logout' className='navbar-item'>
											Logout
										</a>
									)}
								</div>
							</div>
						</div>
					</nav>
				</div>

				<div className='hero-body'>
					<div className='container has-text-centered'>
						<div className='column is-4 is-offset-4'>
							<h3 className='title'>Sign Up</h3>
							<div className='box'>
								{showMessage && (
									<div className='notification is-danger'>
										Invalid details! Could not sign up!
									</div>
								)}
								<form onSubmit={handleSubmit}>
									<div className='field'>
										<div className='control'>
											<input
												className='input is-large'
												type='email'
												name='email'
												placeholder='Email'
												autofocus=''
												required
												onChange={(e) => {
													setEmail(e.target.value)
												}}
											/>
										</div>
									</div>

									<div className='field'>
										<div className='control'>
											<input
												className='input is-large'
												type='text'
												name='name'
												placeholder='Name'
												required
												autofocus=''
												onChange={(e) => {
													setName(e.target.value)
												}}
											/>
										</div>
									</div>

									<div className='field'>
										<div className='control'>
											<input
												className='input is-large'
												type='tel'
												name='contact'
												placeholder='Contact No'
												autofocus=''
												required
												pattern='[0-9]{10}'
												onChange={(e) => {
													setContact(e.target.value)
												}}
											/>
										</div>
									</div>

									<div className='field'>
										<div className='control'>
											<input
												className='input is-large'
												type='password'
												name='password'
												placeholder='Password'
												required
												onChange={(e) => {
													setPassword(e.target.value)
												}}
											/>
										</div>
									</div>

									<button className='button is-block is-info is-large is-fullwidth'>
										Sign Up
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	)
}
